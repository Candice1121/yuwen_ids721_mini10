# Use the cargo-lambda image for building
FROM ghcr.io/cargo-lambda/cargo-lambda:latest as builder

# Create a directory for your application
WORKDIR /app

# Copy your source code into the container
COPY . .

# Build the Lambda function using cargo-lambda
RUN cargo clean && cargo lambda build --release

# Use a new stage for the final image
# copy artifacts to a clean image
FROM public.ecr.aws/lambda/provided:al2

# Create a directory for your lambda function
WORKDIR /yuwen_ids721_mini10

# Copy the bootstrap binary from the builder stage
COPY --from=builder /app/target/ ./ 
# Copy the llama model here 
COPY --from=builder /app/src/pythia-410m-q4_0-ggjt.bin ./ 

# Set the entrypoint for the Lambda function
ENTRYPOINT ["/yuwen_ids721_mini10/bootstrap"]