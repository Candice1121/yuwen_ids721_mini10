# Yuwen_IDS721_mini10 -- Rust Serverless Transformer Endpoint

## Overview
This project implements a serverless transformer endpoint in Rust using AWS Lambda. The Lambda function takes a query as input, processes it using a Rust language model, and returns the result.

## Requirements
- Dockerize Hugging Face Rust transformer
- Deploy container to AWS Lambda
- Implement query endpoint

## Steps

### 1. Install Dependencies
Ensure you have Rust and Docker installed on your machine. Additionally, install the necessary Rust dependencies using Cargo.
```bash
cargo install cargo-lambda
```
### 2. Create New Project
```bash
cargo lambda new <project_name>
```

### 3. Download Model Binary
Download the binary file of the Rust language model from [Hugging Face](https://huggingface.co/rustformers) and store it locally in a directory within your project.

### 4. Update Cargo.toml
Add the required dependencies to your `Cargo.toml` file to include the necessary crates for handling HTTP requests and interacting with AWS Lambda.

### 5. Implement Query Endpoint
Write the code to handle HTTP requests and process queries in the `main.rs` file. Use the `lambda_http` crate to handle HTTP events.

### 6. Test Locally
Test your Lambda function locally using `cargo lambda watch` and `curl`. Send HTTP POST requests to `http://localhost:9000` with sample query data to verify the function's behavior.
![local_test](imgs/local_test.png)

### 7. Build Docker Image
Create a Dockerfile to package your Lambda function and dependencies. Build the Docker image using the provided Dockerfile.

### 8. Upload to Amazon ECR
Set up an Amazon Elastic Container Registry (ECR) repository and push the Docker image containing your Lambda function to the registry.
![ecr](imgs/ecr_steps.png)
![ecr](imgs/ecr_upload.png)

### 9. Create Lambda Function
In the AWS Management Console, create a new Lambda function and configure it to use the container image from ECR. Set the maximum execution time to 5 minutes to allow for longer queries.
![lambda](imgs/lambda_condig.png)

### 10. Test Deployment
Test the deployed Lambda function by invoking it using `curl` or an HTTP client against the generated function URL. Provide sample query data to ensure the function behaves as expected.
![aws_test](imgs/aws_test.png)

## Additional Resources
- [AWS Lambda Documentation](https://docs.aws.amazon.com/lambda)
- [Hugging Face Rust Transformers](https://huggingface.co/rustformers)
- [Cargo Lambda GitHub Repository](https://github.com/awslabs/aws-lambda-rust-runtime)

